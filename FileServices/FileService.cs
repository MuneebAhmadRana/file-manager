﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Assignment1.FileServices
{
    public class FileService
    {
        /// <summary>
        /// Lists all directory files to the Console.
        /// </summary>
        /// <param name="directory"></param>
        public static void ListDirectoryFiles(string directory)
        {
            string[] files = Directory.GetFiles(directory);
            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                Console.WriteLine(fileInfo.Name);
            }
        }

        /// <summary>
        /// Gets files from the provided directory that have the specified extension
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static int GetFilesByExtension(string directory, string extension)
        {
            int count = 0;
            var files = Directory.EnumerateFiles(directory, "*" + extension);
            foreach (var file in files)
            {
                Console.WriteLine(new FileInfo(file).Name);
                count++;
            }
            return count;
        }

        /// <summary>
        /// Returns the name of the file
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string FileName(string directory, string fileName)
        {
            string file = Directory.GetFiles(directory).Where(file => new FileInfo(file).Name == fileName).FirstOrDefault();
            // No file with the specified name was found.
            if (file.Count() < 1)
            {
                throw new FileNotFoundException();
            }
            else
            {
                FileInfo fileInfo = new FileInfo(file);
                Console.WriteLine(fileInfo.Name);
                return fileInfo.Name;
            }
        }

        /// <summary>
        /// Returns the size of the file. The file is found by providing the directory and the file name
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static long FileSize(string directory, string fileName)
        {
            string file = Directory.GetFiles(directory).Where(file => new FileInfo(file).Name == fileName).FirstOrDefault();
            long fileSize = new FileInfo(file).Length;
            Console.WriteLine("The size of " + fileName + " is " + fileSize);
            return fileSize;
        }

        /// <summary>
        /// Returns the total amount of lines present in the file. 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static int FileLines(string directory, string fileName)
        {
            string file = Directory.GetFiles(directory).Where(file => new FileInfo(file).Name == fileName).FirstOrDefault();
            int count = 0;
            using (StreamReader streamReader = new StreamReader(directory + fileName, true))
            {
                while (streamReader.ReadLine() != null)
                {
                    count++;
                }
            }
            Console.WriteLine("The total number of lines in: " + fileName + "are:  " + count);
            return count;
        }

        /// <summary>
        /// Returns the amount of times a specified word is present in a file. Also shows the value inside the Console.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static int SearchWordInFile(string directory, string fileName, string word)
        {
            int wordCount = 0;
            using (StreamReader streamReader = new StreamReader(directory + fileName, true))
            {
                string line = streamReader.ReadLine();
                while (line != null)
                {
                    string[] splitLine = line.ToString().Split(" ");
                    foreach (var split in splitLine)
                    {
                        if (Regex.Replace(split, @"[^\w[()\-]]", "").ToLower() == word.ToLower())
                        {
                            wordCount++;
                        }
                    }
                    line = streamReader.ReadLine();
                }
            }
            Console.WriteLine("The word " + word + " appears " + wordCount + " in the file " + fileName);
            return wordCount;
        }

        /// <summary>
        /// Returns a bool value based on whether the specified word exists in the file or not. 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool WordInFileExists(string directory, string fileName, string word)
        {
            int wordCount = SearchWordInFile(directory, fileName, word);
            if (wordCount < 1)
            {
                Console.WriteLine("The word " + word + " does not exist in file " + fileName);
                return false;
            }
            Console.WriteLine("The word " + word + " does exist in file " + fileName);
            return true;
        }
    }
}