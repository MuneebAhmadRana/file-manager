﻿using Assignment1.CustomExceptions;
using System.IO;
using System.Linq;

namespace Assignment1.FileServices
{
    internal class FileServiceValidation
    {
        /// <summary>
        /// A function that validates the directory
        /// </summary>
        /// <param name="directory"></param>
        public static void ValidateDirectory(string directory)
        {
            CheckDirectoryExists(directory);
            CheckDirectoryEmpty(directory);
        }

        /// <summary>
        /// An overloaded version of Validate that validates both directory and file
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="file"></param>
        public static void ValidateFile(string directory, string file)
        {
            CheckFileExists(directory, file);
        }


        /// <summary>
        /// Checks if file exists in the specified directory. If not, throws a FileNotFoundException
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        private static void CheckFileExists(string directory, string fileName)
        {
            // Checking if the count of files with the fileNames is less than 1. If so, the file does not exist.
            if (Directory.GetFiles(directory).Where(file => new FileInfo(file).Name == fileName).Count() < 1)
            {
                throw new FileNotFoundException();
            }
        }

        /// <summary>
        /// Checks whether the specified directory exists. If not, throws DirectoryNotFoundException
        /// </summary>
        /// <param name="directory"></param>
        private static void CheckDirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                throw new DirectoryNotFoundException("The following directory does not exist on path: " + directory);
            }
        }

        /// <summary>
        /// Checks if directory is empty. If so, throws DirectoryEmptyException
        /// </summary>
        /// <param name="directory"></param>
        private static void CheckDirectoryEmpty(string directory)
        {
            //if files present in the directory are 0, throw DirectoryEmptyException
            string[] files = Directory.GetFiles(directory);
            if (files.Length == 0)
            {
                throw new DirectoryEmptyException("The following directory is empty: " + directory);
            }
        }

        /// <summary>
        /// Checks that the provided extension exists in the specified directory. If not throws ExtensionDoesNotExistException
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="extension"></param>
        public static void ValidateExtension(string directory, string extension)
        {
            bool extensionExists = false;
            string[] files = Directory.GetFiles(directory);
            //If even one file has the specified extension, returns true.
            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                if (fileInfo.Extension == extension)
                {
                    extensionExists = true;
                }
            }
            //If not, throws ExtensionDoesNotExistException. 
            if (!extensionExists)
            {
                throw new ExtensionDoesNotExistException("No files of the following extension found: " + extension + "in the specified directory");
            }
        }
    }
}