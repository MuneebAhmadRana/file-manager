﻿using System.IO;

namespace Assignment1.Logging
{
    public class Logger
    {
        private string fileName;

        public Logger(string fileName)
        {
            this.fileName = fileName;
        }

        /// <summary>
        /// Simple Log function that writes the message to the file Logger is instantiated with. 
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            FileInfo fileInfo = new FileInfo(this.fileName);
            // Appending instead of overwriting textfile. 
            using (StreamWriter streamwriter = new StreamWriter(this.fileName, append: true))
            {
                streamwriter.WriteLine(message);
            }
        }
    }
}