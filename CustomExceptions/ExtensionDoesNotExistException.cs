﻿using System;

namespace Assignment1.CustomExceptions
{
    internal class ExtensionDoesNotExistException : Exception
    {
        //Exception when the extension does not exist.
        public ExtensionDoesNotExistException(string message) : base(message)
        {
        }
    }
}