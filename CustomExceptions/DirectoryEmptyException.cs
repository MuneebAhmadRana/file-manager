﻿using System;

namespace Assignment1.CustomExceptions
{
    internal class DirectoryEmptyException : Exception
    {
        //Exception when the directory is empty. 
        public DirectoryEmptyException(string message) : base(message)
        {
        }
    }
}