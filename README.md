# File Manager
### Indivual Assignment by Muneeb Ahmad Rana

The Assignment has 4 parts. These are as follows.
## 1. User Interaction and Input
A Console Menu that allows the user to decide what they want to do. The Menu persists, allowing the user to continuously select the provided options as much as they desire. 

## 2. File Manipulation
The file manipulation has 3 main tasks.
> Listing all files in the specified directory.

> Listing all files with the specified extension in the provided directory.  

> Several methods that show various info of a specified file present in the specified directory.

Important Note: The directory path must be absolute. 

## 3. Logging
A logging service that keeps track of all the options the user has selected in the aforementioned Menu. An example of the log looks as follows:
>2/16/2021 9:02:11 PM The method ListDirectoryFiles() was used to list files in C:\Users\murana\source\repos\Assignment1\Resources\and it took: 1 ms

## 4. Exception Handling
Good amount of Exception handling was implemented so that the user has a good idea of the situation, in case an error occurs. 
