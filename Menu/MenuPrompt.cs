﻿using System;
using System.Diagnostics;
using System.IO;
using Assignment1.CustomExceptions;
using Assignment1.FileServices;
using Assignment1.Logging;

namespace Assignment1.Menu
{
    public class MenuPrompt
    {
        /// <summary>
        /// Main Prompt function that gets called inside the Program.cs main method.
        /// </summary>
        public static void Prompt()
        {
            try
            {
                //Instantiating a new Logger that will be used throughout the Menu.
                Logger logger = new Logger(@"../../../Logging/Logs.txt");

                Console.WriteLine("\n" + "You are on the main menu " +
                "Please pick one of the following options by using the corresponding numbers\n" +
                "1. List all files in a specified directory\n" +
                "2. List all files of a certain extension in a specified directory\n" +
                "3. Get further details about a specific file in the specified directory\n" +
                "4. Exit\n"
                );
                string output = Console.ReadLine();
                switch (output)
                {
                    case "1": //List all files in a specified directory
                        Prompt1(logger);
                        break;

                    case "2": //List all files based on an extension in a directory.
                        Prompt2(logger);
                        break;

                    case "3": //Various information about a specified file.
                        Prompt3(logger);
                        break;

                    case "4": //Exit
                        Environment.Exit(0);
                        break;

                    default: // In case any numbers other than 1-4 are sent in
                        Console.WriteLine("Please only use numbers 1-4");
                        Prompt();
                        break;
                }
            } //Exceptions.
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorPrompt();
            }
            catch (DirectoryEmptyException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorPrompt();
            }
            catch (ExtensionDoesNotExistException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorPrompt();
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorPrompt();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ErrorPrompt();
            }
            finally
            {
                Logger logger = new Logger(@"../../../Logging/Logs.txt");
                ChangeMenu(logger);
            }
           
        }

        /// <summary>
        /// An error prompt that occurs when an exception is caught.
        /// </summary>
        public static void ErrorPrompt()
        {
            //Error Console.WriteLine to notify the user 
            Console.WriteLine("\n" + "         ---Error---         \n" + "Hmm..an error occured. Would you like to try again?\n" + "Please answer with Yes or No");
            string answer = Console.ReadLine();
            switch (answer.ToLower()) //lowercasing in case user types Yes, No, or YEs etc
            {
                case "yes":  //if yes, start from the beginning
                    Prompt();
                    break;

                case "no": //if no, exit
                    Environment.Exit(0);
                    break;

                default: //if neither, ask again
                    Console.WriteLine("Please use Yes or No");
                    ErrorPrompt();
                    break;
            }
            Prompt();
        }

        /// <summary>
        /// Prompt for 1 switch case in function Prompt(). Used for Listing Directory Files.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="date"></param>
        public static void Prompt1(Logger logger)
        {
            
            Console.WriteLine("\n" + "You are going to list all files in a directory\n" + "Please select the directory (Absolute Path): \n");
            string directory = Console.ReadLine();
            FileServiceValidation.ValidateDirectory(directory);
            //Creating new stopwatch object after the directory has been validated. If it has not, an error will be thrown
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            FileService.ListDirectoryFiles(directory);
            long secondsPassed = stopwatch.ElapsedMilliseconds;
            logger.Log(DateTime.Now.ToString() + " The method ListDirectoryFiles() was used to list files in " + directory + "and it took: " + secondsPassed + " ms");
        }

        /// <summary>
        /// Prompt for 2 switch case in function Prompt(). Used for searching files by extension in a desired directory
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="date"></param>
        public static void Prompt2(Logger logger)
        {
            Console.WriteLine("\n" + "You are now going to list all files with a certain extension\n" + "Please specify the directory you want to search in(Absolute Path): ");
            string directory = Console.ReadLine();
            //Validation for directory 
            FileServiceValidation.ValidateDirectory(directory);
            Console.WriteLine("Please specify your desired extension: ");
            string extension = Console.ReadLine();
            //Validation for extension
            FileServiceValidation.ValidateExtension(directory, extension);
            //Stopwatch object being instantiated and started.
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int fileCount = FileService.GetFilesByExtension(directory, extension);
            long secondsPassed = stopwatch.ElapsedMilliseconds;
            logger.Log(
                DateTime.Now.ToString() + " The function GetFilesByExtension() was executed with extension: " + extension + " in directory: " + directory
                + "\n ----> " + fileCount + " files were found. The function took " + secondsPassed + "ms to execute"
                );
        }

        /// <summary>
        /// Prompt for switch case 3 in function Prompt(). Used for showing information of a specific file.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="date"></param>
        public static void Prompt3(Logger logger)
        {
            Console.WriteLine("\n" + "You are now on File Info Menu: \n" + "Please specify the desired directory(Absolute Path): ");
            string directory = Console.ReadLine();
            //Validating Directory
            FileServiceValidation.ValidateDirectory(directory);
            Console.WriteLine("Please select the file you would like to get information about");
            string fileName = Console.ReadLine();
            //Validating FileName inside the directory.
            FileServiceValidation.ValidateFile(directory, fileName);
            Console.WriteLine("\n" +
                "Please pick one of the following options\n" +
                "1. List the size of " + fileName + "\n" +
                "2. List the number of lines the " + fileName + " has\n" +
                "3. Search if a specific word exists in " + fileName + "\n" +
                "4. Search how many times a certain word occurs in " + fileName + "\n" +
                "5. Search the name of the file" + fileName + ". Not sure why but okay (insert laughing face emoji)" + "\n" +
                "6. Change Menu " + "\n" +
                "7. Exit\n"
                );
            string output = Console.ReadLine();
            switch (output)
            { //For cases 1-5, Stopwatch objects are instantiated to calculate the time it takes
                // for each of the cases respective functions to run
                case "1":
                    Stopwatch stopwatch1 = new Stopwatch();
                    stopwatch1.Start();
                    long fileSize = FileService.FileSize(directory, fileName);
                    long secondsPassed1 = stopwatch1.ElapsedMilliseconds;
                    logger.Log(DateTime.Now.ToString() + " The method FileSize() found file size: " + fileSize + " for file: " + fileName + " in directory: " + directory + ". It took: " + secondsPassed1 + "ms to execute");
                    
                    break;

                case "2":
                    Stopwatch stopwatch2 = new Stopwatch();
                    stopwatch2.Start();
                    int fileCount = FileService.FileLines(directory, fileName);
                    long secondsPassed2 = stopwatch2.ElapsedMilliseconds;
                    logger.Log(DateTime.Now.ToString() + " The method FileLines() found " + fileCount + " lines for file: " + fileName + " in directory: " + directory + ". It took: " + secondsPassed2 + "ms to execute");

                    break;

                case "3":

                    Console.WriteLine("Please type the word to search");
                    string word = Console.ReadLine();
                    Stopwatch stopwatch3 = new Stopwatch();
                    stopwatch3.Start();
                    bool found = FileService.WordInFileExists(directory, fileName, word);
                    long secondsPassed3 = stopwatch3.ElapsedMilliseconds;
                    string result;
                    if (found)
                    {
                        result = "found";
                    }
                    else
                    {
                        result = "did not find";
                    }
                    logger.Log(DateTime.Now.ToString() + " The method WordInFileExists() " + result + " the word" + word + " in file: " + fileName + " in directory: " + directory + ". It took: " + secondsPassed3 + "ms to execute");

                    break;

                case "4":
                    Console.WriteLine("Please type the word to search");
                    string searchWord = Console.ReadLine();
                    Stopwatch stopwatch4 = new Stopwatch();
                    stopwatch4.Start();
                    int wordCount = FileService.SearchWordInFile(directory, fileName, searchWord);
                    long secondsPassed4 = stopwatch4.ElapsedMilliseconds;
                    logger.Log(DateTime.Now.ToString() + " The method SearchWordInFile() found word:" + searchWord + ": " + wordCount + " times in file: " + fileName + " in directory: " + directory + ". It took: " + secondsPassed4 + "ms to execute");

                    break;

                case "5":
                    Console.WriteLine("Finding name for file " + fileName);
                    Stopwatch stopwatch5 = new Stopwatch();
                    stopwatch5.Start();
                    string name = FileService.FileName(directory, fileName);
                    long secondsPassed5 = stopwatch5.ElapsedMilliseconds;
                    logger.Log(DateTime.Now.ToString() + " The method FileName() found name" + name + " for file: " + fileName + " in directory: " + directory + ". It took: " + secondsPassed5 + "ms to execute");
                    break;

                case "6": //Allowing the user to change Menu
                    ChangeMenu(logger);
                    break;

                case "7": //Allowing the user to exit
                    Environment.Exit(0);
                    break;

                default: //Error prompt 
                    ErrorPrompt();
                    break;
            }
        }

        /// <summary>
        /// A prompt to let user decide where they want to go.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="date"></param>
        public static void ChangeMenu(Logger logger)
        {
            Console.WriteLine("\n" +
                "Please pick one of the following options\n" +
                "1. Go to Main Menu\n" +
                "2. Go to File Info Menu\n" +
                "3. Exit.\n"
                );
            string answer = Console.ReadLine();
            switch (answer)
            {
                case "1": //Go to the main menu
                    Prompt();
                    break;

                case "2": //Go to the file methods
                    Prompt3(logger);
                    break;

                case "3":
                    Environment.Exit(0);
                    break;
            }
        }
    }
}